# Rapport d'itération \#0

## Composition de l'équipe

| Nom       | Prénom  | Email                       |
|-----------|---------|-----------------------------|
| COCCHI    | Kévin   | kevin.cocchi@gmail.com      |
| MONSINJON | Nicolas | nicolas.monsinjon@gmail.com |
| BERNEAUX  | Damien  | damien.berneaux@ensiie.fr   |
| BARBIER   | Anthony | phoko.ensiie@gmail.com      |
| GOUEDARD  | Félix   | felix.gouedard@free.fr      |
| MARTIN    | Loïc    | l.martinaudonneau@gmail.com |

## Bilan de l'itération précédente

### Évènements

- **Mise en place de l'environnement de travail :** Git, Trello, linter `eslint` sur Atom avec le [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- Écriture d'un *Hello world* en suivant la méthodologie indiquée

### Taux de complétion de l'itération

2 terminés / 2 prévus = **100%**

### Liste des User Stories terminées

*Aucune*

## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions

- Conseils sur l'utilisation du Git
- Mise en accord sur l'installation des modules sur Atom (linter `eslint`)

### Actions prises pour la prochaine itération

- Pouvoir connecter notre bot aux serveurs Discord
- Récupérer des informations sur l'API des films (type IMBD : à définir)

## Prévisions de l'itération suivante

### Évènements prévus

- **Difficultés éventuelles :** préparation de la semaine de soutenance de stage
- **Évènements à venir :** vacances d'automne

### Titre des User Stories reportées

*Aucune*

### Titre des nouvelles User Stories

- En tant que créateur de la partie, je veux pouvoir démarer une partie en ayant aucune connaissance technique
- En tant qu'utilisateur ou créateur, je veux pouvoir jouer depuis n'importe quel support (ordinateur, tablette, smartphone, ...)
- En tant qu'utilisateur et créateur de la partie, je veux disposer d'une grande base de films afin d'éviter la répétition dans les questions proposées
- En tant qu'utilisateur, je veux pouvoir répondre à la question posée

## Confiance

### Taux de confiance de l'équipe dans l'itération

| &nbsp;    | :( | :! | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 1  | 2  | 0  |

### Taux de confiance de l'équipe pour la réalisation du projet

| &nbsp;    | :( | :! | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |
