# Rapport d'itération

## Composition de l'équipe

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Loïc MARTIN*            |
| **Scrum Master**        | *Anthony BARBIER*        |
| **Dev**                 | *Kévin COCCHI*           |
| **Dev**                 | *Nicolas MONSINJON*      |
| **Dev**                 | *Damien BERNEAUX*        |
| **Dev**                 | *Félix GOUEDARD*         |

## Bilan de l'itération précédente
### Évènements
> Le sprint #1 s'est déroulé de manière smooth.


### Taux de complétion de l'itération
> 5 terminés / 5 prévues = 100%

### Liste des User Stories terminées
> En tant que bot, je veux me connecter à Discord via le module discord.js.
>
> En tant que créateur de la partie, je veux pouvoir démarrer une partie en ayant aucune connaissance technique.
>
> En tant qu'utilisateur, je veux pouvoir répondre à la question posée.
>
> En tant qu'utilisateur ou créateur, je veux pouvoir jouer depuis n'importe quel support (ordinateur, tablette, smartphone, ...).
>
> En tant qu'utilisateur et créateur de la partie, je veux disposer d'une grande base de films afin d'éviter la répétition dans les questions proposées.
>
> En tant qu'utilisateur, je ne veux pas que plusieurs parties puissent se jouer simultanément sur le même chan, par question de visibilité.


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
> L'équipe était satisfaite du travail accompli et commence à constater les avantages de la méthodologie Scrum

### Actions prises pour la prochaine itération
> Faire en sorte que l'on ait tout le monde à la raclette de cohésion pour travailler plus efficacement tous ensemble
>
> Le PO devra réviser plus régulièrement les tâches


### Axes d'améliorations
> Ajouter une colonne de tests sur le Trello pour le PO afin qu'il puisse valider ce qui a été fait
>
> Le PO doit réviser les tâches plus souvent (travail facilité par cette nouvelle colonne)

## Prévisions de l'itération suivante
### Évènements prévus
> Le temps commence à se noircir suite à la fin de ce premier sprint, ce qui présage une légère baisse dans l'humeur des gens et qui se reflètera donc dans une motivation légèrement moins affichée.

### Titre des User Stories reportées

### Titre des nouvelles User Stories
> En tant que personne ayant lancé la partie, je veux pouvoir choisir le nombre de questions afin de moduler la durée de la partie.
>
> En tant qu'utilisateur, je veux avoir la possibilité de demander à passer une question afin de fluidifier le rythme du jeu.
>
> En tant qu'utilisateur qui répond à la question, je veux être en mesure de faire une légère erreur sur ma réponse si elle est proche de la réalité afin de ne pas être entièrement pénalisé.
>
> En tant qu'utilisateur, et/ou créateur de parties, je veux pouvoir me renseigner sur toutes les commandes du bot avec un simple " !help ".
>
> En tant qu'utilisateur, je veux pouvoir voir mon score actuel afin de me rendre compte de ma performance actuelle.
>
> En tant qu'utilisateur, je veux pouvoir marquer des points lorsque ma réponse est correcte.

## Confiance
### Taux de confiance de l'équipe dans l'itération

|           | :(  | :&#124; | :)  | :D  |
|:---------:|:---:|:-------:|:---:|:---:|
| Equipe 13 | *0* |   *0*   | *0* | *6* |

### Taux de confiance de l'équipe pour la réalisation du projet

|           | :(  | :&#124; | :)  | :D  |
|-----------|-----|---------|-----|-----|
| Equipe 13 | *0* | *0*     | *0* | *6* |
