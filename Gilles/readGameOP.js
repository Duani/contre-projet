// Functions
const fs = require('fs');

/*
returns :
- the ID of the OP for the current game in a channel
idChan : string
gameRunning : boolean
*/
const readGameOP = function readGameOP(idChan, gameRunning) {
    // Retrieve the ID of the game OP if game is running
    if (gameRunning) {
        const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${idChan}.json`, 'utf8'));
        return obj.idOp;
    }

    return undefined; // Return undefined as OP's ID if no game is running
};

module.exports = readGameOP;
