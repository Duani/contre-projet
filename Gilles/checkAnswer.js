const fs = require('fs');
/*
returns :
- false if the answer provided does not answer the question
- true if the answer provided is correct
answer : string
question : TODO question object (two strings ?)
*/
const checkAnswer = function checkAnswer(answerProvided) {
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/question.json`));
    return obj.nom[0].toLowerCase() === answerProvided;
};

module.exports = checkAnswer;
