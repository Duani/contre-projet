// index.js

// Classes
// const fs = require('fs');

// Bot's classes
const Discord = require('discord.js');

const token = require(`${__dirname}/token.js`);

// Functions
const createGame = require(`${__dirname}/createGame.js`);
const readGameOP = require(`${__dirname}/readGameOP.js`);
const gameAlreadyExists = require(`${__dirname}/gameAlreadyExists.js`);
const readAnswer = require(`${__dirname}/readAnswer.js`);
const checkAnswer = require(`${__dirname}/checkAnswer.js`);
const requete = require(`${__dirname}/API.js`);
const displayQuestion = require(`${__dirname}/displayQuestion.js`);
const addPoints = require(`${__dirname}/addPoints.js`);
const switchQuestion = require(`${__dirname}/switchQuestion.js`);

const bot = new Discord.Client();


// Connecting the bot
bot.login(token)
    .then(() => console.log('Logged in'))
    .catch(console.error);


// When the bot is ready, we update its informations
bot.on('ready', () => {
    // Change bot's playing game
    bot.user.setGame('!d help')
        .then(() => console.log('Game changed'))
        .catch(console.error);
});


// Action to run as soon as a new message is posted
bot.on('message', async (message) => {
    const chan = message.channel;
    const gameRunning = gameAlreadyExists(chan.id.toString());
    const chanID = chan.id.toString();
    const authorID = message.author.id.toString();

    if (message.content.toLowerCase().startsWith('!gilles start')) {
        // To create a game
        const successCreateGame = createGame(chan.id.toString(), message);

        // display message accordingly
        if (successCreateGame) {
            chan.send('Game created !');
            requete();

            // timeout to ensure displayQuestion will read the json AFTER requete() wrote it
            setTimeout(displayQuestion, 2000, chan);
        } else {
            chan.send('A game is already running in this chan ! Game not created !');
        }
    } else if (message.content.toLowerCase().startsWith('!r') && gameRunning) {
        // Check that the answer provided is correct
        const userAnswer = readAnswer(message, gameRunning);
        const isAnswerCorrect = checkAnswer(userAnswer);
        if (isAnswerCorrect) {
            chan.send('The answer was correct !');

            // add points to player
            addPoints(chanID, authorID);

            // change question
            if (switchQuestion(chan)) {
                chan.send('Switching question !');
            } else {
                chan.send('It\'s finished! Thank you for playing.');
            }
        } else {
            chan.send('The answer was wrong.');
        }
    } else if (message.content.toLowerCase().startsWith('!skip') && gameRunning
               && message.author.id.toString() === readGameOP(chanID, gameRunning)) {
        // Skip the current question and get a new one
        chan.send('Switching question !');
        requete();
        setTimeout(displayQuestion, 500, chan);
    } else if (message.content.toLowerCase().startsWith('!gilles help')) {
        chan.send('Help will be added later.');
    }
});
// end bot.on
