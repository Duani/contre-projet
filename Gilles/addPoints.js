// Functions
const fs = require('fs');

/*
idChan : string
idPlayer : string
*/
const addPoints = function addPoints(idChan, idPlayer) {
    // object for the game in the chan idChan
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${idChan}.json`));

    // if player already has a score for this
    if (obj[idPlayer]) {
        obj[idPlayer] += 1;
    } else {
        obj[idPlayer] = 1;
    }


    // save modifications to the json
    fs.writeFileSync(`${__dirname}/games/${idChan}.json`, JSON.stringify(obj));


    return true;
};

module.exports = addPoints;
