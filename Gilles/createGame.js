// Functions
const fs = require('fs');

const gameAlreadyExists = require(`${__dirname}/gameAlreadyExists.js`);

/*
returns :
- false if the game was not created (i.e. already exists)
- true if created (i.e. doesn't already exist)
idChan : string
idOp : string
*/
const createGame = function createGame(idChan, message) {
    // if a file is already registered for this chan, return false
    if (gameAlreadyExists(idChan)) {
        return false;
    }
    let nbQuestionsMax = 2;
    const idOp = message.author.id.toString();
    const userArray = message.content.toLowerCase().split(' ');
    if (userArray.length >= 3 && userArray[2] >= 1) {
        nbQuestionsMax = userArray[2];
    }
    const remainingQuestions = nbQuestionsMax;
    // the object to write in the json file
    const objectGame = { idOp, nbQuestionsMax, remainingQuestions };

    // Create the games folder if needed
    try {
        fs.statSync(`${__dirname}/games/`);
    } catch (err) {
        // error is thrown if the folder does not exist, so let's create it
        fs.mkdirSync(`${__dirname}/games/`);
    }

    // write the object to the json file for this chan
    fs.writeFileSync(`${__dirname}/games/${idChan}.json`, JSON.stringify(objectGame));


    return true;
};

module.exports = createGame;
