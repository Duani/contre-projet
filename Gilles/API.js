const API_KEY = require(`${__dirname}/api_key.js`);
const fs = require('fs');
const http = require('https');
// const XMLHttpRequest = require('xmlhttprequest');

const fileAlreadyExists = require(`${__dirname}/fileAlreadyExists.js`);


// const GetActorID = async function GetActorID(acteur) {
//     let res = -1;
//     const act = acteur.replace(' ', '+');
//     const xhr = new XMLHttpRequest();
//     xhr.withCredentials = true;
//
//     xhr.addEventListener('readystatechange', () => {
//         if (this.readyState === this.DONE) {
//             res = (JSON.parse(this.responseText)).results[0].id;
//             console.log(res);
//         }
//     });
//
//     xhr.open('GET', `https://api.themoviedb.org/3/search/person?api_key=${API_KEY}&query=${act}`);
//     xhr.send();
//     return res;
// };


// const GetMovieID = function GetMovieID(movie) {
//     let res = -1;
//     const mov = movie.replace(' ', '+');
//     const xhr = new XMLHttpRequest();
//     xhr.withCredentials = true;
//
//     xhr.addEventListener('readystatechange', () => {
//         if (this.readyState === this.DONE) {
//             const data = ((JSON.parse(this.responseText)).results);
//
//             for (let i = 0; i < data.length; i++) {
//                 const obj = data[i];
//                 if (obj.title === movie) {
//                     res = obj.id;
//                 }
//             }
//             console.log(res);
//         }
//     });
//
//     xhr.open('GET', `https://api.themoviedb.org/3/search/person?api_key=${API_KEY}&query=${mov}`);
//     xhr.send();
//     return res;
// };


// const ActorsInMovie = function ActorsInMovie(id) { // liste des acteurs d'un film
//     const res = [];
//     const xhr = new XMLHttpRequest();
//     xhr.withCredentials = true;
//
//     xhr.addEventListener('readystatechange', () => {
//         if (this.readyState === this.DONE) {
//             const data = ((JSON.parse(this.responseText)).cast);
//
//             for (let i = 0; i < data.length; i++) {
//                 const obj = data[i];
//                 res.push(obj.name);
//             }
//             console.log(res);
//         }
//     });
//
//     xhr.open('GET', `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${API_KEY}`); // mettre l'ID du film
//     xhr.send();
//     return res;
// };


// Récupération d'un nombre de films aléatoire pour l'acteur actuel
const moviesByActor = function moviesByActor(retrievedMoviesNumber = 2) {
    if (!fileAlreadyExists(`${__dirname}/question.json`)) {
        return;
    }

    const json = JSON.parse(fs.readFileSync(`${__dirname}/question.json`));

    const randomMovieIDs = [];
    let randomID;

    // Request options to retrieve a random movie given an actor
    const options = {
        method: 'GET',
        hostname: 'api.themoviedb.org',
        port: null,
        path: `/3/person/${json.id}/movie_credits?api_key=${API_KEY}`,
        headers: {},
    };

    // Make the request to retrieve random movies in which an actor played
    const req = http.request(options, (res) => {
        const chunks = [];

        res.on('data', (chunk) => {
            chunks.push(chunk);
        });

        res.on('end', () => {
            const body = Buffer.concat(chunks); // Request's result body
            const { cast } = JSON.parse(body); // Movies in which the actor starred

            // Number of movies in which the actor starred
            const numberMovies = cast.length;

            // Pick random movies
            for (let i = 0; i < retrievedMoviesNumber; i++) {
                // Pick a new random ID until not already picked
                do {
                    randomID = Math.floor(Math.random() * (numberMovies - 1));
                } while (randomMovieIDs.indexOf(randomID) >= 0);

                randomMovieIDs[i] = randomID; // Store that ID so that it doesn't get picked twice
                json.film[0][i] = cast[randomID].title; // Push title to question object
            }

            // Write movies informations to question file
            fs.writeFileSync(`${__dirname}/question.json`, JSON.stringify(json));
        });
    });

    req.write('{}');
    req.end();
};


const randomActor = function randomActor() {
    const page = Math.floor(Math.random() * 10) + 1;
    const obj = { id: [], nom: [], film: [[]] };

    // Request options to retrieve a random actor
    const options = {
        method: 'GET',
        hostname: 'api.themoviedb.org',
        port: null,
        path: `/3/person/popular?api_key=${API_KEY}&page=${page}`,
        headers: {},
    };

    // Make the request to retrieve a random actor
    const req = http.request(options, (res) => {
        const chunks = [];

        res.on('data', (chunk) => {
            chunks.push(chunk);
        });

        res.on('end', () => {
            const body = Buffer.concat(chunks); // Request's result body

            // Number of actor results per page
            const numberResults = JSON.parse(body).results.length;

            // Pick random actor
            const actorPickedNumber = Math.floor(Math.random() * (numberResults - 1));
            const actor = JSON.parse(body).results[actorPickedNumber];

            obj.id.push(actor.id); // Push actor ID to question object
            obj.nom.push(actor.name); // Push actor name to question object
            console.log(`Current answer : ${actor.name}`); // Log current actor name

            // Write actor informations to question file
            fs.writeFileSync(`${__dirname}/question.json`, JSON.stringify(obj));
            moviesByActor(2); // Add 2 films from this actor
        });
    });

    req.write('{}');
    req.end();

    return 'Random actor picked !';
};


const requete = function requete() {
    randomActor();
    return 'ok requete';
};


module.exports = requete;
