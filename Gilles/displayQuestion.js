const fs = require('fs');

const fileAlreadyExists = require(`${__dirname}/fileAlreadyExists`);

const displayQuestion = function displayQuestion(chan) {
    if (fileAlreadyExists(`${__dirname}/question.json`)) {
        const obj = JSON.parse(fs.readFileSync(`${__dirname}/question.json`));
        const moviesCount = obj.film[0].length;

        // Build question string
        let questionDisplay = 'Quel acteur a joué dans ';
        for (let i = 0; i < moviesCount - 1; i++) {
            questionDisplay += obj.film[0][i];
            questionDisplay += (i === moviesCount - 2) ? ' et ' : ', ';
        }
        questionDisplay += obj.film[0][moviesCount - 1];
        questionDisplay += ' ?';

        chan.send(questionDisplay); // Write question in channel
    } else {
        console.log('Error reading question.json file in displayQuestion.js');
    }
};

module.exports = displayQuestion;
