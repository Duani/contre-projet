const fs = require('fs');

/*
path : string
*/
const fileAlreadyExists = function fileAlreadyExists(path) {
    return fs.existsSync(path);
};

module.exports = fileAlreadyExists;
