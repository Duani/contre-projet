/*
returns :
- the answer of the user or an empty string
message : Message object
gameRunning : boolean
*/
const readAnswer = function readAnswer(message, gameRunning) {
    // Verify that a game is running
    if (gameRunning) {
        // Verify that the input is not only '!r' but '!r <answer>'
        const userAnswerArray = message.content.toLowerCase().split(' ');
        userAnswerArray.shift();
        const userAnswerString = userAnswerArray.join(' ');
        return userAnswerString;
    }

    return false;
};

module.exports = readAnswer;
