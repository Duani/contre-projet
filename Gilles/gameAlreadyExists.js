// Functions
const fileAlreadyExists = require(`${__dirname}/fileAlreadyExists.js`);

/*
idChan : string
*/
const gameAlreadyExists = function gameAlreadyExists(idChan) {
    return fileAlreadyExists(`${__dirname}/games/${idChan}.json`);
};

module.exports = gameAlreadyExists;
