// Functions
const fs = require('fs');

const requete = require(`${__dirname}/API.js`);
const displayQuestion = require(`${__dirname}/displayQuestion.js`);

const switchQuestion = function switchQuestion(chan) {
    const idChan = chan.id.toString();
    const objectGame = JSON.parse(fs.readFileSync(`${__dirname}/games/${idChan}.json`));
    if (objectGame.remainingQuestions > 1) {
        objectGame.remainingQuestions -= 1;
        requete();
        setTimeout(displayQuestion, 500, chan);
        fs.writeFileSync(`${__dirname}/games/${idChan}.json`, JSON.stringify(objectGame));
        return true;
    }
    // TODO affiche les scores
    fs.unlinkSync(`${__dirname}/games/${idChan}.json`);
    fs.unlinkSync(`${__dirname}/question.json`);
    return false;
};
module.exports = switchQuestion;
